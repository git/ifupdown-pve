include /usr/share/dpkg/default.mk

PACKAGE=ifupdown

SRCDIR=src
UPSTREAM=https://salsa.debian.org/debian/ifupdown.git
BUILDDIR=$(SRCDIR)-$(DEB_VERSION_UPSTREAM)

DEB=$(PACKAGE)_$(DEB_VERSION_UPSTREAM_REVISION)_$(DEB_BUILD_ARCH).deb
DBG_DEB=$(PACKAGE)-dbgsym_$(DEB_VERSION_UPSTREAM_REVISION)_$(DEB_BUILD_ARCH).deb
DEBS=$(DEB) $(DBG_DEB)

all: $(DEB)
	@echo $(DEB)

.PHONY: deb upload distclean clean dinstall

buildir: $(BUILDDIR)
$(BUILDDIR):
	rm -rf $(BUILDDIR) $(BUILDDIR).tmp
	cp -a $(SRCDIR)/ $(BUILDDIR).tmp
	mv $(BUILDDIR).tmp $(BUILDDIR)

deb: $(DEB)
$(DEB) $(DBG_DEB): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -rfakeroot -b -uc -us
	lintian $(DEBS)

upload: $(DEBS)
	tar cf - $(DEBS) | ssh -X repoman@repo.proxmox.com -- upload --product pve,pmg,pbs --dist bullseye

distclean: clean
clean:
	rm -rf $(SRCDIR)-*/ *.deb *.changes *.dsc *.buildinfo

dinstall: deb
	dpkg -i $(DEB)
